/**
 *Name: Alejandro Monroy Reyes
 *USC loginid: monroyre
 *CSCI 455 PA1
 *Fall 2016
*/

/**
 *  Class CoinTossSimulatorTester
 *  A class to test the CoinTossSimulator class.
 *  
 *  Tests the different methods of the class and print the results. 
 */
public class CoinTossSimulatorTester {
    
    public static void main(String[] args)
    {
        CoinTossSimulator coinToss = new CoinTossSimulator();
        
        System.out.println("After Constructor:");
        printResults(coinToss);
               
        System.out.println("After run (1)");
        coinToss.run(1);
        printResults(coinToss);
        
        System.out.println("After run (10)");
        coinToss.run(10);
        printResults(coinToss);
        
        System.out.println("After run (100)");
        coinToss.run(100);
        printResults(coinToss);
       
        System.out.println("After run (1000)");
        coinToss.run(1000);
        printResults(coinToss);
        
        System.out.println("After run (10000)");
        coinToss.run(10000);
        printResults(coinToss);
        
        System.out.println("After reset:");
        coinToss.reset();
        printResults(coinToss);
        
        System.out.println("After run (1000000)");
        coinToss.run(1000000);
        printResults(coinToss);
        
        System.out.println("After run (1)");
        coinToss.run(1);
        printResults(coinToss);

        System.out.println("After run (10)");
        coinToss.run(10);
        printResults(coinToss);
        
        System.out.println("After run (100)");
        coinToss.run(100);
        printResults(coinToss);
        
        System.out.println("After reset:");
        coinToss.reset();
        printResults(coinToss);
        
        
    }
    
    /**
        A method to print the number of trials, the number of times we got each outcome, 
        and calls the method isSumCorrect. 
    */
    private static void printResults(CoinTossSimulator coinToss)
    {
        System.out.println("Number of trials= " + coinToss.getNumTrials());
        System.out.println("Two-head tosses= " + coinToss.getTwoHeads());
        System.out.println("Two-tail tosses= " + coinToss.getTwoTails());
        System.out.println("One-head one-tail tosses= " + coinToss.getHeadTails());
        System.out.println("Tosses add up correctly?" + isSumCorrect(coinToss));
        System.out.println("");
    }
    /**
    A method that determine if the sum of the three outcomes is equal to the number of trials and returns 
    the result to printResults method.  
    */
    
    private static boolean isSumCorrect(CoinTossSimulator coinToss)
    {
        boolean result;
        int sumToss = coinToss.getTwoHeads() + coinToss.getTwoTails() + coinToss.getHeadTails();
        
        if(sumToss == coinToss.getNumTrials())
        {
            result = true;
        }
        else
        {
            result = false;
        }
        return result;
    }
}
