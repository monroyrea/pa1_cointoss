/**
 *Name: Alejandro Monroy Reyes
 *USC loginid: monroyre
 *CSCI 455 PA1
 *Fall 2016
 */

/**
 * Class CoinSimViewer
 * Creates a Frame window to display the graphical results of the simulation of tossing 
 * two coins n number of times. 
 * 
 * Also, it allows the user to enter how many trials to perform. And tests if the input 
 * is not less or equal to zero. 
 */

import javax.swing.JFrame;
import java.util.Scanner;

public class CoinSimViewer {
    public static void main(String[] args)
    {
        int trials = 0;
        JFrame frame1= new JFrame();
        Scanner scan1 = new Scanner(System.in);

        frame1.setSize(800, 500);
        frame1.setTitle("CoinSimulator");
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Tests the user input to determine if the introduced number is greater than zero or not.
        while(trials<=0)    
        {
            System.out.print("Enter number of trials: ");
            trials = scan1.nextInt();
            if(trials <=0)
            {
                System.out.println("ERROR: Number entered must be greater than 0");
            }
        }
        scan1.close();
        
        CoinTossSimulator coinSimulator = new CoinTossSimulator();
        coinSimulator.run(trials);
        
        CoinSimComponent component = new CoinSimComponent(coinSimulator);
        frame1.add(component);
        
        frame1.setVisible(true);       
    }
}
