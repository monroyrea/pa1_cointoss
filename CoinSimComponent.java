/**
 *Name: Alejandro Monroy Reyes
 *USC loginid: monroyre
 *CSCI 455 PA1
 *Fall 2016
 */

/**
 *  Class CoinSimComponent
 *  A class that recovers Graphics2D, constructs a bar object and calls a method of the 
 *  Bar class two draw the labeled bars. 
 */

import javax.swing.JComponent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.text.DecimalFormat;

public class CoinSimComponent extends JComponent{
	
    //Defines the bar width size and the margin in the bottom of the label. 
    private static final int BAR_WIDTH = 100;
    private static final int LABEL_BOTTOM_MARGIN = 25; 
            
    private CoinTossSimulator coinToss;
    private DecimalFormat twoDecimals;
	
    /**
    Creates a coinSimComponent and initialize the object coinToss and twoDecimals.
    */
    public CoinSimComponent(CoinTossSimulator coinToss)
    {
        this.coinToss = coinToss;
        this.twoDecimals = new DecimalFormat("#.00"); 
    }
	
    /**
    A class to define the variables to construct a Bar object. 
    */
    public void paintComponent(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        
        //Bottom of the label will be the total height of the window minus 25 pixels
        int labelBottom = getHeight()- LABEL_BOTTOM_MARGIN;  

        //Sets the left side of the bar depending on the size of the frame. 
        int twoHeadsBarLeft = (getWidth()/4) - (BAR_WIDTH/2);        
        int headTailsBarLeft = (getWidth()/4*2) -(BAR_WIDTH/2);
        int twoTailsBarLeft = (getWidth()/4*3) -(BAR_WIDTH/2);

        //Get the number of times each outcome was given. 
        int numUnitsTwoHeadsBar = coinToss.getTwoHeads();
        int numUnitsHeadTailsBar = coinToss.getHeadTails();
        int numUnitsTwoTailsBar = coinToss.getTwoTails();
        
        int numTrials = coinToss.getNumTrials();    
        
        double unitsPerPixel = (double)numTrials / ((getHeight()-LABEL_BOTTOM_MARGIN));
        
        //Sets the percentage to a two decimals String. 
        String percentageTwoHeads = twoDecimals.format(numUnitsTwoHeadsBar/ (double)numTrials*100);
        String percentageHeadTails = twoDecimals.format(numUnitsHeadTailsBar/ (double)numTrials*100);
        String percentageTwoTails = twoDecimals.format(numUnitsTwoTailsBar/ (double)numTrials*100);
        
        //Defines the label text. 
        String twoHeadsBarLabel = "Two Heads: " + coinToss.getTwoHeads() + "(" + percentageTwoHeads + "%)";
        String headTailsBarLabel = "A Head and a Tail: " + coinToss.getHeadTails() + "(" + percentageHeadTails + "%)";
        String twoTailsBarLabel =  "Two Tails: " + coinToss.getTwoTails() + "(" + percentageTwoTails + "%)";

        //Construct the bar objects with the previously defined parameters.
        Bar twoHeadsBar = new Bar(labelBottom,twoHeadsBarLeft,BAR_WIDTH,numUnitsTwoHeadsBar,unitsPerPixel,Color.RED,twoHeadsBarLabel);
        Bar headTailsBar = new Bar(labelBottom,headTailsBarLeft,BAR_WIDTH,numUnitsHeadTailsBar,unitsPerPixel,Color.GREEN,headTailsBarLabel);
        Bar twoTailsBar = new Bar(labelBottom,twoTailsBarLeft,BAR_WIDTH,numUnitsTwoTailsBar,unitsPerPixel,Color.BLUE,twoTailsBarLabel);
        
        //Calls the draw method of the class Bar.
        twoHeadsBar.draw(g2);
        headTailsBar.draw(g2);
        twoTailsBar.draw(g2);
    }
}
