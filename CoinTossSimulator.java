/**
 *Name: Alejandro Monroy Reyes
 *USC loginid: monroyre
 *CSCI 455 PA1
 *Fall 2016
 */

/**
 * class CoinTossSimulator
 * 
 * Simulates trials of tossing two coins and allows the user to access the
 * cumulative results.
 * 
 * NOTE: we have provided the public interface for this class.  Do not change
 * the public interface.  You can add private instance variables, constants, 
 * and private methods to the class.  You will also be completing the 
 * implementation of the methods given. 
 * 
 * Invariant: getNumTrials() = getTwoHeads() + getTwoTails() + getHeadTails()
 * 
 */

import java.util.Random;

public class CoinTossSimulator {

    private int numTrials;
    private int twoHeads;
    private int twoTails;
    private int headTails;
    private Random rand;
   /**
      Creates a coin toss simulator with no trials done yet.
   */
    public CoinTossSimulator() {
        this.numTrials = 0;
        this.twoHeads = 0;
        this.twoTails = 0;
        this.headTails = 0;
        this.rand = new Random();
    }


   /**
      Runs the simulation for numTrials more trials. Multiple calls to this
      without a reset() between them add these trials to the simulation
      already completed.
      
      @param numTrials  number of trials to for simulation; must be >= 0
    */
    public void run(int numTrials) {
        this.numTrials = this.numTrials + numTrials;
        boolean try1;   
        boolean try2;
	 
        for(int i = 0; i<numTrials; i++)
        {	
            try1 = this.rand.nextBoolean(); //Generates a random boolean an assign it to variable try1
            try2 = this.rand.nextBoolean(); //Generates a random boolean an assign it to variable try2
            
            if(try1 == true && try2 == true) //Increment variable twoHeads by 1 when both try1 and try2 are true. 
            {
                this.twoHeads++;
            }
            else if(try1 == false && try2 == false) //Increment variable twoTails by 1 when both try1 and try2 are false. 
            {
                this.twoTails++;
            }
            else //Increment variable headTails by 1 when both try1 and try2 are different. 
            {
                this.headTails++;
            } 
        }
    }

   /**
      Get number of trials performed since last reset.
   */
    public int getNumTrials() {
        return this.numTrials; // Return the number of trials. 
    }


   /**
      Get number of trials that came up two heads since last reset.
   */
    public int getTwoHeads() {
        return this.twoHeads; // Return the number of trials where two heads were obtained.
    }


   /**
     Get number of trials that came up two tails since last reset.
   */  
    public int getTwoTails() {
        return this.twoTails; // DReturn the number of trials where two tails were obtained.
    }


   /**
     Get number of trials that came up one head and one tail since last reset.
   */
    public int getHeadTails() {
        return this.headTails; //Return the number of trials where one head and one tail was obtained.
    }


   /**
      Resets the simulation, so that subsequent runs start from 0 trials done.
    */
    public void reset() {   
        this.numTrials = 0;
        this.twoHeads = 0;
        this.twoTails = 0;
        this.headTails = 0;
    }
}
